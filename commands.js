const env = require('./.env');
const Telegraf = require('telegraf')

const bot = new Telegraf(env.token)

bot.start( async ctx => {
    await ctx.reply(`Seja bem vindo, ${ctx.update.message.from.first_name}\nAvise se precisar de /ajuda`);
})

bot.command("ajuda", ctx => {
    ctx.reply("/ajuda: Vou te mostrar as opções\n"+
    "/ajuda2: para testar via hears\n"+
    "/op2: Opção genérica\n"+
    "/op3: Não sei o que estou fazendo!")
});

bot.hears("/ajuda2", ctx => {
    ctx.reply("Oi, chame o /ajuda kk")
});


bot.hears(/\/op/i, ctx => {
    ctx.reply("Olar!")
});

bot.launch()