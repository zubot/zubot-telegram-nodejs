const env = require('./.env');
const Telegraf = require('telegraf')

const bot = new Telegraf(env.token)
bot.start( async(ctx) => {
  const from = ctx.update.message.from
  console.log(from);
  await ctx.reply(`What's up? 🙃`);
  await ctx.replyWithHTML(`I'm listening you <b>${from.first_name}</b>`);
  await ctx.replyWithMarkdown(`Fork me on *Gitlab*\n `+ '`gitlab.com/zubot`\n\n 🤩🤩🤩');
  await ctx.replyWithPhoto({source: `${__dirname}/pic0.jpg`});
  await ctx.replyWithPhoto('https://cdn-images-1.medium.com/max/1600/1*ak9D7YqxUeuX1Um2wmh6Ig.jpeg',
  {caption:'kkkk'});
  await ctx.replyWithLocation(-75.1004857,123.3170698);
  //await ctx.replyWithVideo('URL');
})

bot.on('text', ctx =>
    ctx.reply(`Text received: '${ctx.update.message.text}'`));

bot.launch()