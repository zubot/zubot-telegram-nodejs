const env = require('./.env');
const Telegraf = require('telegraf')

const bot = new Telegraf(env.token)
bot.start((ctx) => {
  const from = ctx.update.message.from
  console.log(from);
  ctx.reply(`Welcome, ${from.first_name}!`);
})

bot.on('text', ctx =>
    ctx.reply(`Text received: '${ctx.update.message.text}'`));


bot.on('location', ctx => {
    const location = ctx.update.message.location;
    console.log(location);
    ctx.reply(`Here you are! \nLat: ${location.latitude} \nLon: ${location.longitude}`)
});

bot.on('contact', ctx => {
    const contact = ctx.update.message.contact;
    console.log(contact);
    ctx.reply(`Ya! I'll remember \nName: ${contact.first_name} \nPhone Number: ${contact.phone_number}`)
});

bot.on('voice', ctx => {
    const voice = ctx.update.message.voice;
    console.log(voice);
    ctx.reply(`Seriously? I don't have time to listen to ${voice.duration} seconds of audio`)
});

bot.on('photo', ctx => {
    const photo = ctx.update.message.photo;
    console.log(photo);
    ctx.reply(`Nice pic!`);
});

bot.on('sticker', ctx => {
    const sticker = ctx.update.message.sticker;
    console.log(sticker);
    ctx.reply(`Emoji from your sticker: ${sticker.emoji}\nSticker Set: ${sticker.set_name}`);
});

bot.launch()