const env = require('./.env');
const Telegraf = require('telegraf')
const axios = require("axios");
const bot = new Telegraf(env.token)


bot.on('voice', async (ctx, next) => {
  const id = ctx.update.message.voice.file_id;
  const res = await axios.get(`${env.apiURL}/getFile?file_id=${id}`)
  ctx.replyWithVoice({url: `${env.apiFileURL}/${res.data.result.file_path}`})

});

bot.on( 'photo', async (ctx, next) => {
    const id = ctx.update.message.photo[0].file_id;
    const res = await axios.get(`${env.apiURL}/getFile?file_id=${id}`);
    ctx.replyWithPhoto({url: `${env.apiFileURL}/${res.data.result.file_path}`})
})


bot.launch()