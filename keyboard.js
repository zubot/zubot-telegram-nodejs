const env = require('./.env');
const Telegraf = require('telegraf')
const Markup = require('telegraf/markup')

const bot = new Telegraf(env.token)

const tecladoFood = Markup.keyboard([
    ["🌭 Cachorro quente", "🍔 Hamburguer", "🍕 Pizza"],
    ["🌮 Taco", "🌯 Burrito"],
    ["🍣 Sushi", "🥞 Panquecas"],
    ["🍨 Sorvete", "🍿 Pipoca"]
]).resize().extra();

bot.start( async ctx => {
    await ctx.reply(`Seja bem vindo, ${ctx.update.message.from.first_name}`);
    await ctx.reply(`Qual bebida você prefere?`,
            Markup.keyboard(["Coca-Cola","Pepsi"]).resize().oneTime().extra())
})

bot.hears(["Coca-Cola","Pepsi"], async ctx => {
    await ctx.reply(`Legal! Eu também gosto de ${ctx.match}`);
    await ctx.reply(`E qual comida?`, tecladoFood);
});

bot.hears("🍣 Sushi", ctx => {
    ctx.reply("Top, mas eu não gosto de peixe cru... 😝")
});

bot.hears("🍔 Hamburguer", ctx => {
    ctx.reply("Aí Sim!!! 😍")
});

bot.on("text", ctx => {
    ctx.reply("Legal!!! 😉")
});

bot.launch()