const env = require('./.env');
const Telegraf = require('telegraf')
const moment = require("moment");


const bot = new Telegraf(env.token)
bot.hears('pizza', (ctx, next) => {
  ctx.reply('Quero!');
});

bot.hears(['chuchu', 'salada'], (ctx, next) => {
    ctx.reply('Passo!');
});

bot.hears('🐷', (ctx, next) => {
    ctx.reply('Hmmm... bacon! 😋');
});

bot.hears(/burguer/i, (ctx, next) => {
    ctx.reply('Yes, please!');
});

bot.hears([/salada/i,/brocolis/i], (ctx, next) => {
    ctx.reply('NOOOOO!');
});

bot.hears(/(\d{2}\/\d{2}\/\d{4})/, (ctx, next) => {
    moment.locale('pt-BR');
    const data = moment(ctx.match[1], 'DD/MM/YYYY');
    ctx.reply(`${ctx.match[1]} cai em ${data.format('dddd')}`);
});
bot.launch()